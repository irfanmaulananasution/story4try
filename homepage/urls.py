from django.urls import path
from . import views

app_name = 'homepage'

urlpatterns = [
	path('', views.home, name=''),
	path('Blog', views.blog, name='blog'),
	path('Blog_1', views.blog_1, name='blog_1'),
]